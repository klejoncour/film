# Mini Projet PHP - Films
By Kévin LE JONCOUR - Quentin BIHET  
url : [http://projet-film.no-ip.org](http://projet-film.no-ip.org)  
mirroir : [http://projet-film.max.st](http://projet-film.max.st)  
Dépot Git : [https://bitbucket.org/klejoncour/film](https://bitbucket.org/klejoncour/film)

## Technologies utilisées

* PHP 5.4.10
* Framework Symfony 2.3.7

## Description

Le but était de réaliser un site web permettant la visualisation de Films.

Il est possible:

* D'afficher la liste des films présents sur le site
* De faire une recherche selon différents critères
* D'exporter la liste des films appartenant à un genre donnée:
    * En xml --> Uniquement la liste des films
    * En zip --> Le zip contenant un fichier XML de la liste des films ainsi que les images de ces films
* D'importer des films à partir d'un fichier XML. Lors de l'import du fichier, celui-ci est vérifié grâce à une DTD

## Installation

1. Création d'une base de données
2. Importation des données avec le script SQL "databases/database.sql"
3. Modification du fichier de configuration "app/config/parameters.yml" avec les informations de connéxion à la base de données
4. Création des liens symboliques : # php app/console assets:install web --symlink
5. Nettoyer le cache : # php app/console cache:clear --env=prod
6. Attribution des droits : # chmod -R 755 web && chmod -R 755 app/cache && chmod -R 755 app/logs

## Architecture

/  
|  
|- Accueil  
|- Liste des films  
|- Rechercher  
|- Export  
|- Import  
|- Film  