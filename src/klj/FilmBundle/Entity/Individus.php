<?php

namespace klj\FilmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Individus
 *
 * @ORM\Table(name="individus")
 * @ORM\Entity
 */
class Individus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="code_indiv", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codeIndiv;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=20, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=20, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="nationalite", type="string", length=20, nullable=true)
     */
    private $nationalite;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_naiss", type="integer", nullable=true)
     */
    private $dateNaiss;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_mort", type="integer", nullable=true)
     */
    private $dateMort;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Films", mappedBy="refCodeActeur")
     */
    private $refCodeFilm;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->refCodeFilm = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

    /**
     * Get codeIndiv
     *
     * @return integer 
     */
    public function getCodeIndiv()
    {
        return $this->codeIndiv;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Individus
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Individus
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set nationalite
     *
     * @param string $nationalite
     * @return Individus
     */
    public function setNationalite($nationalite)
    {
        $this->nationalite = $nationalite;
    
        return $this;
    }

    /**
     * Get nationalite
     *
     * @return string 
     */
    public function getNationalite()
    {
        return $this->nationalite;
    }

    /**
     * Set dateNaiss
     *
     * @param integer $dateNaiss
     * @return Individus
     */
    public function setDateNaiss($dateNaiss)
    {
        $this->dateNaiss = $dateNaiss;
    
        return $this;
    }

    /**
     * Get dateNaiss
     *
     * @return integer 
     */
    public function getDateNaiss()
    {
        return $this->dateNaiss;
    }

    /**
     * Set dateMort
     *
     * @param integer $dateMort
     * @return Individus
     */
    public function setDateMort($dateMort)
    {
        $this->dateMort = $dateMort;
    
        return $this;
    }

    /**
     * Get dateMort
     *
     * @return integer 
     */
    public function getDateMort()
    {
        return $this->dateMort;
    }

    /**
     * Add refCodeFilm
     *
     * @param \klj\FilmBundle\Entity\Films $refCodeFilm
     * @return Individus
     */
    public function addRefCodeFilm(\klj\FilmBundle\Entity\Films $refCodeFilm)
    {
        $this->refCodeFilm[] = $refCodeFilm;
    
        return $this;
    }

    /**
     * Remove refCodeFilm
     *
     * @param \klj\FilmBundle\Entity\Films $refCodeFilm
     */
    public function removeRefCodeFilm(\klj\FilmBundle\Entity\Films $refCodeFilm)
    {
        $this->refCodeFilm->removeElement($refCodeFilm);
    }

    /**
     * Get refCodeFilm
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefCodeFilm()
    {
        return $this->refCodeFilm;
    }
    
    public function __toString(){
	    return strtoupper($this->nom)." ".$this->prenom;
    }
}