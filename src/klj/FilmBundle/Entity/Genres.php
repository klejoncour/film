<?php

namespace klj\FilmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Genres
 *
 * @ORM\Table(name="genres")
 * @ORM\Entity
 */
class Genres
{
    /**
     * @var integer
     *
     * @ORM\Column(name="code_genre", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codeGenre;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_genre", type="string", length=50, nullable=true)
     */
    private $nomGenre;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Films", mappedBy="refCodeGenre")
     */
    private $refCodeFilm;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->refCodeFilm = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

    /**
     * Get codeGenre
     *
     * @return integer 
     */
    public function getCodeGenre()
    {
        return $this->codeGenre;
    }

    /**
     * Set nomGenre
     *
     * @param string $nomGenre
     * @return Genres
     */
    public function setNomGenre($nomGenre)
    {
        $this->nomGenre = $nomGenre;
    
        return $this;
    }

    /**
     * Get nomGenre
     *
     * @return string 
     */
    public function getNomGenre()
    {
        return $this->nomGenre;
    }

    /**
     * Add refCodeFilm
     *
     * @param \klj\FilmBundle\Entity\Films $refCodeFilm
     * @return Genres
     */
    public function addRefCodeFilm(\klj\FilmBundle\Entity\Films $refCodeFilm)
    {
        $this->refCodeFilm[] = $refCodeFilm;
    
        return $this;
    }

    /**
     * Remove refCodeFilm
     *
     * @param \klj\FilmBundle\Entity\Films $refCodeFilm
     */
    public function removeRefCodeFilm(\klj\FilmBundle\Entity\Films $refCodeFilm)
    {
        $this->refCodeFilm->removeElement($refCodeFilm);
    }

    /**
     * Get refCodeFilm
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefCodeFilm()
    {
        return $this->refCodeFilm;
    }
}