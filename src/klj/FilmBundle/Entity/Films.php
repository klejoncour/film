<?php

namespace klj\FilmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use klj\FilmBundle\Resources\Allocine\Allocine;

/**
 * Films
 *
 * @ORM\Table(name="films")
 * @ORM\Entity
 */
class Films
{
    /**
     * @var integer
     *
     * @ORM\Column(name="code_film", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codeFilm;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_original", type="string", length=50, nullable=true)
     */
    private $titreOriginal;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_francais", type="string", length=50, nullable=true)
     */
    private $titreFrancais;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=20, nullable=true)
     */
    private $pays;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="integer", nullable=true)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="duree", type="integer", nullable=true)
     */
    private $duree;

    /**
     * @var string
     *
     * @ORM\Column(name="couleur", type="string", length=10, nullable=true)
     */
    private $couleur;

    /**
     * @var \klj\FilmBundle\Entity\Individus
     *
     * @ORM\ManyToOne(targetEntity="Individus", inversedBy="refCodeFilm")
     * @ORM\JoinColumn(name="realisateur", referencedColumnName="code_indiv")
     */
    private $realisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=20, nullable=true)
     */
    private $image;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Individus", inversedBy="refCodeFilm")
     * @ORM\JoinTable(name="acteurs",
     *   joinColumns={
     *     @ORM\JoinColumn(name="ref_code_film", referencedColumnName="code_film")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ref_code_acteur", referencedColumnName="code_indiv")
     *   }
     * )
     */
    private $refCodeActeur;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Genres", inversedBy="refCodeFilm")
     * @ORM\JoinTable(name="classification",
     *   joinColumns={
     *     @ORM\JoinColumn(name="ref_code_film", referencedColumnName="code_film")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ref_code_genre", referencedColumnName="code_genre")
     *   }
     * )
     */
    private $refCodeGenre;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->refCodeActeur = new \Doctrine\Common\Collections\ArrayCollection();
        $this->refCodeGenre = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

    /**
     * Get codeFilm
     *
     * @return integer 
     */
    public function getCodeFilm()
    {
        return $this->codeFilm;
    }

    /**
     * Set titreOriginal
     *
     * @param string $titreOriginal
     * @return Films
     */
    public function setTitreOriginal($titreOriginal)
    {
        $this->titreOriginal = $titreOriginal;
    
        return $this;
    }

    /**
     * Get titreOriginal
     *
     * @return string 
     */
    public function getTitreOriginal()
    {
        return $this->titreOriginal;
    }

    /**
     * Set titreFrancais
     *
     * @param string $titreFrancais
     * @return Films
     */
    public function setTitreFrancais($titreFrancais)
    {
        $this->titreFrancais = $titreFrancais;
    
        return $this;
    }

    /**
     * Get titreFrancais
     *
     * @return string 
     */
    public function getTitreFrancais()
    {
        return $this->titreFrancais;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return Films
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    
        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set date
     *
     * @param integer $date
     * @return Films
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return integer 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set duree
     *
     * @param integer $duree
     * @return Films
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;
    
        return $this;
    }

    /**
     * Get duree
     *
     * @return integer 
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set couleur
     *
     * @param string $couleur
     * @return Films
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
    
        return $this;
    }

    /**
     * Get couleur
     *
     * @return string 
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set realisateur
     *
     * @param \klj\FilmBundle\Entity\Individus $realisateur
     * @return Films
     */
    public function setRealisateur(\klj\FilmBundle\Entity\Individus $realisateur)
    {
        $this->realisateur = $realisateur;
    
        return $this;
    }

    /**
     * Get realisateur
     *
     * @return \klj\FilmBundle\Entity\Individus 
     */
    public function getRealisateur()
    {
        return $this->realisateur;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Films
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add refCodeActeur
     *
     * @param \klj\FilmBundle\Entity\Individus $refCodeActeur
     * @return Films
     */
    public function addRefCodeActeur(\klj\FilmBundle\Entity\Individus $refCodeActeur)
    {
        $this->refCodeActeur[] = $refCodeActeur;
    
        return $this;
    }

    /**
     * Remove refCodeActeur
     *
     * @param \klj\FilmBundle\Entity\Individus $refCodeActeur
     */
    public function removeRefCodeActeur(\klj\FilmBundle\Entity\Individus $refCodeActeur)
    {
        $this->refCodeActeur->removeElement($refCodeActeur);
    }

    /**
     * Get refCodeActeur
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefCodeActeur()
    {
        return $this->refCodeActeur;
    }

    /**
     * Add refCodeGenre
     *
     * @param \klj\FilmBundle\Entity\Genres $refCodeGenre
     * @return Films
     */
    public function addRefCodeGenre(\klj\FilmBundle\Entity\Genres $refCodeGenre)
    {
        $this->refCodeGenre[] = $refCodeGenre;
    
        return $this;
    }

    /**
     * Remove refCodeGenre
     *
     * @param \klj\FilmBundle\Entity\Genres $refCodeGenre
     */
    public function removeRefCodeGenre(\klj\FilmBundle\Entity\Genres $refCodeGenre)
    {
        $this->refCodeGenre->removeElement($refCodeGenre);
    }

    /**
     * Get refCodeGenre
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefCodeGenre()
    {
        return $this->refCodeGenre;
    }
    
    
    public function setImageFilm()
    {
	    $allocine = new Allocine();
	    
		$result = $allocine->search($this->titreOriginal);
		$result = json_decode($result);
		
		$nbResult = @$result->feed->count;		
		if($nbResult > 0 && @$result->feed->movie[0]->poster->href){
			$result = $result->feed->movie;
			$image = $result[0]->poster->href;
			$img = 'img/'.$this->codeFilm.".jpg";
			file_put_contents($img, file_get_contents($image));
			
			$this->image = $img;
			
		}else{
			$this->image = "img/no-image.jpg";
		}
		
		return $this;
    }

}