<?php
namespace klj\FilmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use klj\FilmBundle\Entity\Films;
use klj\FilmBundle\Entity\Genres;
use klj\FilmBundle\Entity\Individus;

class DefaultController extends Controller
{

    /**
     * Affiche la liste des 20 derniers films ajoutés sur la page d'accueil
     * 
     * @return index.html.twig
     */
    public function indexAction()
    {
    	$param = array();
    	$param["title"] = "Accueil";
    	$repositoryFilm = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Films');
    	$param["films"] = $repositoryFilm->findBy(array(),array("codeFilm"=>"desc"), 20, 0);
        return $this->render('kljFilmBundle:Default:index.html.twig', $param);
    }
    
    
    /**
     * Affiche les informations d'un film.
     * 
     * @param int $id Identifiant du film
     * @return film.html.twig
     */
    public function getFilmAction($id)
    {
    	$param = array();
    	$param["title"] = "Détail";
    	
    	$repositoryFilm = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Films');
    	$param["film"] = $repositoryFilm->findOneByCodeFilm($id);
    	
        return $this->render('kljFilmBundle:Default:film.html.twig', $param);
    }

    /**
     * Affiche la liste des films
     * 
     * @return list.html.twig
     */
    public function listAction()
    {
    	$param = array();
    	$param["title"] = "Liste des films";
    	$repositoryFilm = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Films');
    	$param["films"] = $repositoryFilm->findBy(array(),array("titreFrancais"=>"asc"));
        return $this->render('kljFilmBundle:Default:list.html.twig', $param);
    }
    
    /**
     * Affiche le formulaire de recherche et son résultat.
     * 
     * @return search.html.twig
     */
    public function searchAction()
    {
    	$param = array();
    	$param["title"] = "Rechercher";
    	$param["isResults"] = false;
    	
    	$repositoryGenre = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Genres');
    	$repositoryFilm = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Films');
    	
    	// Récupération de la liste des genres
		$tabGenres = $repositoryGenre->findBy(array(),array("nomGenre"=>"asc"));
    	$listGenres = array(0=>"Aucun");
       	foreach($tabGenres as $genre)
       		$listGenres[$genre->getCodeGenre()] = $genre->getNomGenre();
    	
    	
    	$tabFilms = $repositoryFilm->findAll();
  
    	// Récupérzation de la liste des réalisateurs, pays et titres et de la date minimale
    	$listRealisateurs = array();
    	$listPays = array();
    	$listTitre = array();
    	$minDate = date('Y');
       	foreach($tabFilms as $film)
       	{
       		if(!isset($listRealisateurs[$film->getRealisateur()->getCodeIndiv()]))
	       		$listRealisateurs[$film->getRealisateur()->getCodeIndiv()] = $film->getRealisateur()->getNom()." ".$film->getRealisateur()->getPrenom();
       		
       		if(!isset($listPays[$film->getPays()]))
	       		$listPays["".$film->getPays()] = $film->getPays();
       		
       		if($film->getDate() < $minDate) $minDate = $film->getDate();
       		
       		$listTitre[]=$film->getTitreFrancais();
    	}
    	
    	$param["titres"] = $listTitre;
    	
    	asort($listPays);
    	$listPays[] = "Aucun";
    	
    	asort($listRealisateurs);
    	$listRealisateurs[0] = "Aucun";
    	
    	// Définition de la liste des dates
    	$listDates = array(0=>"Aucun");
    	foreach(range(date('Y'),$minDate) as $key=>$val) $listDates[$val] = $val;
    	
    	// Création du formulaire de recherche
    	$form = $this->createFormBuilder()
    		->add('titre', 'text', array("required"=>false,	"attr"=>array("placeholder"=>"Titre")))
            ->add('pays', 'choice', array("choices"=>$listPays,'preferred_choices'=>array('0'),"multiple"=>true))
            ->add('date', 'choice', array('choices' => $listDates,'preferred_choices'=>array('0'),"multiple"=>true))
            ->add('realisateur', 'choice', array("choices"=>$listRealisateurs,'preferred_choices'=>array('0'),"multiple"=>true))
            ->add('genre', 'choice', array("choices"=>$listGenres,'preferred_choices'=>array('0'),"multiple"=>true))
            ->getForm();

		// On récupère la requête.
		$request = $this->get('request');
		// On vérifie qu'elle est de type « POST ».
		if( $request->getMethod() == 'POST' )
		{
			// On fait le lien Requête <-> Formulaire.
			$form->bind($request);
			// On vérifie que les valeurs rentrées sont correctes.
			if( $form->isValid() )
			{
    	    	// Création de la requête
    	    	$tabFilms = $repositoryFilm->createQueryBuilder('f');
    	    	
    	    	// Filtre sur le titre du film
    	    	$tabFilms->where('f.titreFrancais LIKE :titre');
    	    	$tabFilms->setParameter('titre',  "%".$form->get('titre')->getData()."%");
    	    	
    	    	//Filtre sur le pays
       	    	if($form->get('pays')->getData() && $form->get('pays')->getData()[0] != "0")
       	    	{	
       	    		$tabFilms->andWhere('f.pays IN (:pays)');
       	    		$tabFilms->setParameter('pays',  $form->get('pays')->getData());
       	    	}
       	    	
       	    	// Filtre sur la date	
    	    	if($form->get('date')->getData() && $form->get('date')->getData()[0] != 0)
    	    	{
    	    		$tabFilms->andWhere('f.date IN (:date)');
       	    		$tabFilms->setParameter('date',  $form->get('date')->getData());
       	    	}
    	    	
    	    	// Filtre sur le réalisateur
    	    	if($form->get('realisateur')->getData() && $form->get('realisateur')->getData()[0] != 0)
    	    	{
    	    		$tabFilms->andWhere('f.realisateur IN (:realisateur)');
       	    		$tabFilms->setParameter('realisateur',  $form->get('realisateur')->getData());
    	    	}
    	    	
    	    	// Execution de la requête
				$tabFilms = $tabFilms->orderBy("f.titreFrancais","asc")->getQuery()->getResult();
				
				// Filtre sur le genre
				if($form->get('genre')->getData() && $form->get('genre')->getData()[0] != 0)
				{
					$tabGenres = $form->get('genre')->getData();
					$tab = array();
					foreach($tabFilms as $film){
						foreach($film->getRefCodeGenre() as $genre){
							if(in_array($genre->getCodeGenre(), $tabGenres)){
								$tab[] = $film;
								break;
							}
						}
					}
					$tabFilms = $tab;
				}
								
				$param["films"] = $tabFilms;
				$param["isResults"] = true;
			}
    	}
    	$param["form"]=$form->createView();

        return $this->render('kljFilmBundle:Default:search.html.twig', $param);
    }
    
    
    /**
     * Affiche une liste déroulante pour sélectionner le genre à exporter, puis génère le fichier XML.
     * 
     * @return export.html.twig
     */
    public function exportAction()
    {
    	
    	$param = array();
    	$param["title"] = "Export";
    	
    	$repositoryFilm = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Films');
    	$repositoryGenre = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Genres');
    	    	
		$tabGenres = $repositoryGenre->findBy(array(), array('nomGenre'=>'asc'));
    	
    	$listChoice = array();
       	foreach($tabGenres as $genre){
       		$listChoice[$genre->getCodeGenre()] = $genre->getNomGenre();
    	}
    	
    	$form = $this->createFormBuilder()
            ->add('genre', 'choice', array('choices' => $listChoice,"required"=>true, "attr"=>array("placeholder"=>"Genre")))
            ->add('format', 'choice', array('choices' => array("xml"=>"XML (Données)","zip"=>"ZIP (Données + Images)"),"required"=>true))
            ->getForm();

		// On récupère la requête.
		$request = $this->get('request');
		// On vérifie qu'elle est de type « POST ».
		if( $request->getMethod() == 'POST' )
		{
			// On fait le lien Requête <-> Formulaire.
			$form->bind($request);
			// On vérifie que les valeurs rentrées sont correctes.
			if( $form->isValid() )
			{	
			
				$format = $form->get('format')->getData();
				$genreId = $form->get('genre')->getData();
				$param = array();			
		    	$param["films"] = array();
		    	$param["format"] = $format;
		    	
		    	$tabFilms = $repositoryFilm->findAll();
		    	foreach($tabFilms as $film){
		    		$tabGenres = $film->getRefCodeGenre();
		    		foreach($tabGenres as $genre){
			    		if($genre->getCodeGenre() == $genreId) $param["films"][] = $film;
		    		}
		    	}
		    	
		    	$content = $this->renderView('kljFilmBundle:Default:export.xml.twig',$param);
		    	$filename = "EXPORT_".$this->sanitize(trim($repositoryGenre->findOneByCodeGenre($genreId)->getNomGenre())).".xml";
		    	//echo $content;
		        
		        if($format == "xml"){
			        $res = new Response(str_replace("&#039;","'",$content));
			        $res->headers->set('Content-Type', 'application/xml');
			        $res->setCharset('ISO-8859-1');
			        $res->headers->set('Content-Disposition', 'attachment; filename='.$filename);
			        return $res;
		        }else if($format == "zip"){
		        	$tempDirName = time();
			        
			        // Création d'un repertoire temporaire
			        if(!is_dir("temp"))
			        	mkdir("temp");
			        	
			        mkdir("temp/".$tempDirName);
			        
			        // Ajout du fichier .xml
			        file_put_contents("temp/".$tempDirName."/".$filename, str_replace("&#039;","'",$content));
			        
			        // Ajout des images
			        mkdir("temp/".$tempDirName."/img");
			        foreach($param["films"] as $film){
				        if($film->getImage() != "")
				        	copy($film->getImage(),"temp/".$tempDirName."/".$film->getImage());
			        }
			        
			        // Création de l'archive .zip
			        $source_dir = "temp/".$tempDirName."/";
					$zip_file = "temp/".$tempDirName.".zip";
					$file_list = $this->listDirectory($source_dir);
					 
					$zip = new \ZipArchive();
					if ($zip->open($zip_file, \ZIPARCHIVE::CREATE) === true) {
						foreach ($file_list as $file) {
							if ($file !== $zip_file) {
								$zip->addFile($file, substr($file, strlen($source_dir)));
							}
						}
						$zip->close();
						
						// Suppression du répertoire temporaire
						$this->rmdir_recursive($source_dir);
					}
			        
			        $filename = "EXPORT_".$this->sanitize(trim($repositoryGenre->findOneByCodeGenre($genreId)->getNomGenre())).".zip";
			        
			        $res = new Response(file_get_contents($zip_file));
			        $res->headers->set('Content-Type', 'application/x-zip');
			        $res->headers->set('Content-Disposition', 'attachment; filename='.$filename);
			        
			        // Supression du fichier .zip
			        unlink($zip_file);
			        
			        return $res;
			    }
		    }
    	}
    	$param["form"]=$form->createView();

        return $this->render('kljFilmBundle:Default:export.html.twig', $param);
    }


    /**
     * Affiche un formulaire permettant l'import de données au format XML.
     * 
     * @return import.html.twig
     */
    public function importAction()
    {
    	
    	$repositoryFilm = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Films');
    	$repositoryGenre = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Genres');
    	$repositoryIndividu = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Individus');
    	$em = $this->getDoctrine()->getManager();
    	
    	$param = array();
    	$param["title"] = "Importer";
    	//$repositoryFilm = $this->getDoctrine()->getManager()->getRepository('kljFilmBundle:Films');
    	//$param["films"] = $repositoryFilm->findBy(array(),array("codeFilm"=>"desc"), 20, 0);

    	$form = $this->createFormBuilder()
            ->add('fichier', 'file')
            ->getForm();

		// On récupère la requête.
		$request = $this->get('request');
		$param["error"] = false;
		$param["dtd_text"] = nl2br(htmlentities(file_get_contents('bundles/kljfilm/import.dtd')));
		// On vérifie qu'elle est de type « POST ».
		if( $request->getMethod() == 'POST' )
		{
			// On fait le lien Requête <-> Formulaire.
			$form->bind($request);
			// On vérifie que les valeurs rentrées sont correctes.
			if( $form->isValid() )
			{
				// On récupère le fichier transférer
				$file=$form['fichier']->getData();
				
				if(!empty($file)){
					// On récupère l'extension du fichier
					$extension = $file->guessExtension();
	
					// On vérifie que le fichier est bien un ".xml"
					if ($extension == 'xml') {
					
						// On récupère le contenu du fichier
						$contentFile = file_get_contents($file->getpathName());
					
						// On vérifie que le premier noeud est le noeud "films"
						if (strstr($contentFile, '<films>')) {
							// On prend le contenu de la DTD	
							$dtd = file_get_contents('bundles/kljfilm/import.dtd');
							// On déclare le noeud principal du fichier ".xml"
							$root = 'films';
	
							//On vérifie le fichier XML grâce au DTD
							$systemId = 'data://text/plain;base64,'.base64_encode($dtd);
	
							$old = new \DOMDocument();
							$old->loadXML($contentFile);
	
							$creator = new \DOMImplementation();
							$doctype = $creator->createDocumentType($root, null, $systemId);
							$new = $creator->createDocument(null, null, $doctype);
							$new->encoding = "utf-8";
	
							$oldNode = $old->getElementsByTagName($root)->item(0);
							$newNode = $new->importNode($oldNode, true);
							$new->appendChild($newNode);
	
							// Si le fichier est valide on fait l'insertion dans la base
							if (@$new->validate()) {
							    $tabData = json_decode(json_encode((array) simplexml_load_string($contentFile)),1);
							    
							    // Dans le cas où il n'y a qu'un film, le xml ne renvoi pas un array
							    if(!isset($tabData["film"][0])){
							    	$tabTemp = $tabData["film"];
							    	$tabData["film"]= array();
							    	$tabData["film"][] = $tabTemp;
							    }
							    
							    $tabImageFilm = array();
							    foreach($tabData["film"] as $film){
	
							    	// Ajout du film non éxistant
								    $filmEntity = $repositoryFilm->findOneByTitreOriginal($film["titreOriginal"]);
								    if(count($filmEntity) == 0){
										$filmEntity = new Films();
										$filmEntity->setTitreOriginal($film["titreOriginal"]);
										$filmEntity->setPays($film["pays"]);
										$filmEntity->setDate($film["date"]);
										$filmEntity->setDuree($film["duree"]);
										$filmEntity->setCouleur($film["couleur"]);
										
										// Ajout réalisateur
										$realisateurEntity = $repositoryIndividu->findOneBy(array("nom"=>$film["realisateur"]["nom"],"prenom"=>$film["realisateur"]["prenom"]),array());
									    if(count($realisateurEntity) == 0){
										    $realisateurEntity = new Individus();
											$realisateurEntity->setNom($film["realisateur"]["nom"]);
											$realisateurEntity->setPrenom($film["realisateur"]["prenom"]);
													
											$realisateurEntity = $em->merge($realisateurEntity);
											$em->persist($realisateurEntity);
	
										}
										$filmEntity->setRealisateur($realisateurEntity);
										
										// Champs facultatifs
										if(isset($film["titreFrancais"]))
											$filmEntity->setTitreFrancais($film["titreFrancais"]);
										else
											$filmEntity->setTitreFrancais($film["titreOriginal"]);
										
										if(!empty($film["image"]))
											$filmEntity->setImage($film["image"]);
										else 
											$tabImageFilm[] = $filmEntity;
										
										// Ajout des genres non éxistants
								    	if(isset($film["genres"]["genre"])){
								    		$tabGenres = $film["genres"]["genre"];
								    		
								    		// Dans le cas où il n'y a qu'un genre, le xml ne renvoi pas un array
								    		if(!is_array($tabGenres)){
										    	$tabTemp = $tabGenres;
										    	$tabGenres = array();
										    	$tabGenres[] = $tabTemp;
										    }
									    	
									    	foreach($tabGenres as $genre){
									    		$genreEntity = $repositoryGenre->findOneByNomGenre($genre);
									    		if(count($genreEntity) == 0){
										    		$genreEntity = new Genres();
													$genreEntity->setNomGenre($genre);
													
													$genreEntity = $em->merge($genreEntity);
													$em->persist($genreEntity);
												}
												$filmEntity->addRefCodeGenre($genreEntity);
									    	}
		
								    	}
									    
									    // Ajout des acteurs non éxistants	
									    $acteurs = array();							    
									    if(isset($film["acteurs"]["acteur"])){
										    $tabActeurs = $film["acteurs"]["acteur"];
										    
										    // Dans le cas où il n'y a qu'un acteur, le xml ne renvoi pas un array
										    if(!isset($tabActeurs[0])){
										    	$tabTemp = $tabActeurs;
										    	$tabActeurs = array();
										    	$tabActeurs[] = $tabTemp;
										    }
										    
										    foreach($tabActeurs as $acteur){
									    		$acteurEntity = $repositoryIndividu->findOneBy(array("nom"=>$acteur["nom"],"prenom"=>$acteur["prenom"]),array());
									    		if(count($acteurEntity) == 0){
										    		$acteurEntity = new Individus();
													$acteurEntity->setNom($acteur["nom"]);
													$acteurEntity->setPrenom($acteur["prenom"]);
													
													$acteurEntity = $em->merge($acteurEntity);
													$em->persist($acteurEntity);
												}
												
												$filmEntity->addRefCodeActeur($acteurEntity);
									    	}
									    }
									    
										// On persiste le film, le save se fera en une seule fois
										$em->persist($filmEntity);
									}
							    }
							    
							    // On save les films dans la base
							    $em->flush();
							    
							    // Les images étant nommées en fonction du codeFilm, les films doivent être présents dans la base, on charge donc les images dans un second temps
							    if(count($tabImageFilm) != 0){
								    foreach($tabImageFilm as $film){
							    		$film->setImageFilm();
							    		$em->persist($film);
							    	}
							    	$em->flush();
							    }
							    
							    $param["message"] = "Le fichier a bien été importé.";
							    
							// Si le fichier n'est pas valide on informe l'utilisateur
							} else {
							    $param["error"] = true;
							    $param["message"] = "Le fichier ne respect pas le format nécessaire !";
							}
						 } else {
							 $param["error"] = true;
							 $param["message"] = "Le fichier ne respect pas le format nécessaire !";
						}
					} else {
						 $param["error"] = true;
						 $param["message"] = "Le fichier n'est pas au format XML !";
					}
				} else {
					 $param["error"] = true;
					 $param["message"] = "Veuillez choisir un fichier !";
				}
			}
		}
		$param["form"]=$form->createView();
        return $this->render('kljFilmBundle:Default:import.html.twig', $param);
    }
    
    /**
    * Génère une chaine de caractères propre pour les URLS ou noms de fichiers
    */
    private function sanitize($string, $force_lowercase = true, $anal = false)
    {
	    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
	                   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
	                   "â€”", "â€“", ",", "<", ".", ">", "/", "?");
	    $clean = trim(str_replace($strip, "", strip_tags($string)));
	    $clean = preg_replace('/\s+/', "-", $clean);
	    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
	    return ($force_lowercase) ?
	        (function_exists('mb_strtolower')) ?
	            mb_strtolower($clean, 'UTF-8') :
	            strtolower($clean) :
	        $clean;
    }

    private function listDirectory($dir)
    {
   		$result = array();
   		$root = scandir($dir);
	    foreach($root as $value) {
	    	if($value === '.' || $value === '..') {
	        	continue;
	        }
	        
	        if(is_file("$dir$value")) {
	        	$result[] = "$dir$value";
	        	continue;
	        }
	        
	        if(is_dir("$dir$value")) {
	        	$result[] = "$dir$value/";
	        }
	        
	        foreach($this->listDirectory("$dir$value/") as $value)
	        {
	        	$result[] = $value;
	        }
	    }
	    return $result;
	}
    
    private function rmdir_recursive($dir) 
    {
	    foreach(scandir($dir) as $file) {
	        if ('.' === $file || '..' === $file) continue;
	        if (is_dir("$dir/$file")) $this->rmdir_recursive("$dir/$file");
	        else unlink("$dir/$file");
	    }
	    rmdir($dir);
	}
	
}