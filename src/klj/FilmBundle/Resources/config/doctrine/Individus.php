<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Individus
 *
 * @ORM\Table(name="individus")
 * @ORM\Entity
 */
class Individus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="code_indiv", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codeIndiv;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=20, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=20, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="nationalite", type="string", length=20, nullable=true)
     */
    private $nationalite;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_naiss", type="integer", nullable=true)
     */
    private $dateNaiss;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_mort", type="integer", nullable=true)
     */
    private $dateMort;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Films", mappedBy="refCodeActeur")
     */
    private $refCodeFilm;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->refCodeFilm = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}
