<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Genres
 *
 * @ORM\Table(name="genres")
 * @ORM\Entity
 */
class Genres
{
    /**
     * @var integer
     *
     * @ORM\Column(name="code_genre", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codeGenre;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_genre", type="string", length=50, nullable=true)
     */
    private $nomGenre;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Films", mappedBy="refCodeGenre")
     */
    private $refCodeFilm;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->refCodeFilm = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}
