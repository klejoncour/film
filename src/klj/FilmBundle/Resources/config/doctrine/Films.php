<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Films
 *
 * @ORM\Table(name="films")
 * @ORM\Entity
 */
class Films
{
    /**
     * @var integer
     *
     * @ORM\Column(name="code_film", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codeFilm;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_original", type="string", length=50, nullable=true)
     */
    private $titreOriginal;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_francais", type="string", length=50, nullable=true)
     */
    private $titreFrancais;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=20, nullable=true)
     */
    private $pays;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="integer", nullable=true)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="duree", type="integer", nullable=true)
     */
    private $duree;

    /**
     * @var string
     *
     * @ORM\Column(name="couleur", type="string", length=10, nullable=true)
     */
    private $couleur;

    /**
     * @var integer
     *
     * @ORM\Column(name="realisateur", type="integer", nullable=true)
     */
    private $realisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=20, nullable=true)
     */
    private $image;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Individus", inversedBy="refCodeFilm")
     * @ORM\JoinTable(name="acteurs",
     *   joinColumns={
     *     @ORM\JoinColumn(name="ref_code_film", referencedColumnName="code_film")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ref_code_acteur", referencedColumnName="code_indiv")
     *   }
     * )
     */
    private $refCodeActeur;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Genres", inversedBy="refCodeFilm")
     * @ORM\JoinTable(name="classification",
     *   joinColumns={
     *     @ORM\JoinColumn(name="ref_code_film", referencedColumnName="code_film")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ref_code_genre", referencedColumnName="code_genre")
     *   }
     * )
     */
    private $refCodeGenre;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->refCodeActeur = new \Doctrine\Common\Collections\ArrayCollection();
        $this->refCodeGenre = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}
